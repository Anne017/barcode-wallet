/**
 * Wait before the DOM has been loaded before initializing the Ubuntu UI layer
 */

var UI = null;

window.onload = function () {

    function addClass(elem, className) {
        elem.className += ' ' + className;
    };

    function removeClass(elem, className) {
        elem.className = elem.className.replace(className, '');
    };

    UI = new UbuntuUI();
    UI.init();

    // Detect if Cordova script is uncommented or not and show the appropriate status.
    var hasCordovaScript = false;
    var scripts = [].slice.call(document.querySelectorAll('script'));
    scripts.forEach(function (element) {
        var attributes = element.attributes;
        if (attributes && attributes.src && attributes.src.value.indexOf('cordova.js') !== -1) {
            hasCordovaScript = true;
        }
    });

    var cordovaLoadingIndicator = document.querySelector('.load-cordova');
    if (!hasCordovaScript) {
        removeClass(document.querySelector('.ko-cordova'), 'is-hidden');
        addClass(cordovaLoadingIndicator, 'is-hidden');
    }

    // Add an event listener that is pending on the initialization
    //  of the platform layer API, if it is being used.
    document.addEventListener("deviceready", function() {


        console.log (hasCordovaScript);
        console.log (cordova);
        console.log (navigator);
        console.log (JSON.stringify (Object.keys(cordova)));
        console.log (JSON.stringify (Object.keys(navigator)));
        if (console && console.log) {
            console.log('Platform layer API ready');

        }
        removeClass(document.querySelector('.ok-cordova'), 'is-hidden');
        addClass(cordovaLoadingIndicator, 'is-hidden');
    }, false);
    populateCardList ();
    UI.button("Edit-Button").click (editCard);
    UI.button("New-Button").click (newCard);
    UI.button("Save-Button").click (saveCard);
    UI.button("Delete-Button").click (deleteCard);

    UI.pagestack.push ("Card-List-Page");
};

var cards = null;
var currentCardIndex = -1;

function populateCardList ()
{
    var _json =  loadCardsFromFile ();
    var cardList = UI.list ('[id="card-list"]');

    cards = JSON.parse (_json);

    for (var i = 0; i < cards.cards.length; i++)
    {
        addCardToList (cardList, i, cards.cards[i].name);
    }
}

function addCardToList (cardList, index, cardName)
{
    cardList.append (cardName, "", index.toString (), showCardDetailsTab, index);
}

function showCardDetailsTab (button, index)
{
    if (cards === null) return;

    currentCardIndex = index;
    refreshCardDetails ();
    UI.pagestack.push("card-page");
}

function refreshCardDetails ()
{
    var name = cards.cards[currentCardIndex].name;
    var value = cards.cards[currentCardIndex].value;

    var page = UI.page ("card-page");
    page.element().setAttribute ("data-title", name);

    var Barcode = document.getElementById ("Barcode");
    var Barcode_Value = document.getElementById ("Barcode_Value");

    Barcode.innerHTML = stringToBarcode (value);
    Barcode_Value.innerHTML = value;
}

function stringToBarcode (str)
{
	if (str.length & 1 === 1) return;
    return "&Ograve;" + typeCToTypeB (str) + "&Oacute;";
}

function calculateCheck (code)
{
	var CheckSum = 105;
	for (var i = 0; i < code.length; i++)
	{
		CheckSum += code[i] * (i + 1);
    }
	return CheckSum % 103;
}

function getCharacterFromValue (v)
{
	if (v >= 0 && v <= 94)
	{
		return String.fromCharCode (v + 32);
	}
	else if (v >= 95 && v <= 102)
	{
		return String.fromCharCode (v + 100)
	}
	return "";
}

function typeCToTypeB (code)
{
	var codeArray = [];
	var codeString = "";
	
	for (var i = 0; i < code.length; i+=2)
	{
		var value = parseInt (code.substring (i, i+2));
		if (isNaN (value)) return "";
		codeArray.push (value);
		codeString += getCharacterFromValue (value);
	}
	
	return codeString += getCharacterFromValue (calculateCheck (codeArray));
}

function editCard (index)
{
    modifyCard ("Edit Card");
}

function newCard ()
{
    var New_Card = new Object ();
    New_Card.name = "";
    New_Card.value= "";
    currentCardIndex = cards.cards.length;
    cards.cards.push (New_Card);

    UI.list ('[id="card-list"]').append (" ", "", currentCardIndex,  showCardDetailsTab, currentCardIndex);

    modifyCard ("New Card");
}

function modifyCard (title)
{
    if (currentCardIndex >= cards.cards.length) return;

    var page = UI.page("edit-page");
    page.element ().setAttribute ("data-title", title);

    var cardNameInput = document.getElementById("Edit-Card-Name");
    var cardValueInput = document.getElementById("Edit-Card-Value");

    cardNameInput.value = cards.cards[currentCardIndex].name;
    cardValueInput.value = cards.cards[currentCardIndex].value;

    UI.pagestack.push ("edit-page");
}

function saveCard ()
{
    var cardNameInput = document.getElementById("Edit-Card-Name");
    var cardValueInput = document.getElementById("Edit-Card-Value");

    cards.cards[currentCardIndex].name = cardNameInput.value;
    cards.cards[currentCardIndex].value = cardValueInput.value;

    refreshCardDetails ();

    UI.list('[id="card-list"]').at(currentCardIndex+1).firstChild.innerHTML = cardNameInput.value;

    UI.pagestack.pop();
}

function deleteCard ()
{
    UI.list('[id="card-list"]').remove (currentCardIndex+1);
    cards.cards.pop (currentCardIndex);

    UI.pagestack.clear ();
    UI.pagestack.push ("Card-List-Page");
}

function loadCardsFromFile ()
{
    console.log (navigator.file.dataDirectory);
    return '{ "cards" : [ { "name": "Subcard", "value": "6338450342814594" },' +
            '{ "name" : "ClubCard", "value" : "634004025022630478" },'+
            '{ "name" : "ClubCard", "value" : "9794025022630478" }] }';
}
