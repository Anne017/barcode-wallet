# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/matthew/card-wallet/platforms/ubuntu/native/build/cordovaubuntuplugin_automoc.cpp" "/home/matthew/card-wallet/platforms/ubuntu/native/build/CMakeFiles/cordovaubuntuplugin.dir/cordovaubuntuplugin_automoc.cpp.o"
  "/home/matthew/card-wallet/platforms/ubuntu/build/src/cordova.cpp" "/home/matthew/card-wallet/platforms/ubuntu/native/build/CMakeFiles/cordovaubuntuplugin.dir/src/cordova.cpp.o"
  "/home/matthew/card-wallet/platforms/ubuntu/build/src/cordova_config.cpp" "/home/matthew/card-wallet/platforms/ubuntu/native/build/CMakeFiles/cordovaubuntuplugin.dir/src/cordova_config.cpp.o"
  "/home/matthew/card-wallet/platforms/ubuntu/build/src/cordova_whitelist.cpp" "/home/matthew/card-wallet/platforms/ubuntu/native/build/CMakeFiles/cordovaubuntuplugin.dir/src/cordova_whitelist.cpp.o"
  "/home/matthew/card-wallet/platforms/ubuntu/build/src/cplugin.cpp" "/home/matthew/card-wallet/platforms/ubuntu/native/build/CMakeFiles/cordovaubuntuplugin.dir/src/cplugin.cpp.o"
  "/home/matthew/card-wallet/platforms/ubuntu/build/src/qmlplugin.cpp" "/home/matthew/card-wallet/platforms/ubuntu/native/build/CMakeFiles/cordovaubuntuplugin.dir/src/qmlplugin.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  "QT_WIDGETS_LIB"
  "QT_XML_LIB"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "/home/matthew/card-wallet/platforms/ubuntu/build"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64"
  "/usr/include/x86_64-linux-gnu/qt5/QtQuick"
  "/usr/include/x86_64-linux-gnu/qt5/QtQml"
  "/usr/include/x86_64-linux-gnu/qt5/QtNetwork"
  "/usr/include/x86_64-linux-gnu/qt5/QtXml"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
