const translations = 
{
	"Main-Page-Title" :
	{
		"en" : "Cards",
		"fr" : "Cartes",
		"de" : "Karten",
		"it" : "Carte",
		"es" : "tarjetas",
		"nl" : "Kaarten",
		"da" : "Kort",
		"no" : "Kort",
		"sv" : "Kort",
		"fi" : "Kortit",
		"cs" : "Karty",
		"lt" : "Kortelės",
		"pt" : "Cartões",
		"pl" : "Karty",
		"ru" : "Карты",
		"zh" : "卡",
		"hi" : "कार्ड",
		"ja" : "カード",
		"el" : "Κάρτες",
		"ar" : "بطاقات",
		"cy" : "cardiau"
	},
	
	"New-Page-Title" :
	{
		"en" : "New Card",
		"fr" : "Nouvelle Carte",
		"de" : "Neue Karte",
		"it" : "Nuova Carte",
		"es" : "Nueva Tarjeta",
		"nl" : "Nieuwe Kaart",
		"da" : "Nyt Kort",
		"no" : "Nytt Kort",
		"sv" : "Nytt Kort",
		"fi" : "Uusi Kortin",
		"cs" : "Nová Karta",
		"lt" : "Nauja kortelė",
		"pt" : "Novo Cartão",
		"pl" : "Nowa karta",
		"ru" : "Новый карты",
		"zh" : "新卡",
		"hi" : "नए कार्ड",
		"ja" : "新しいカード",
		"el" : "νέα Κάρτα",
		"ar" : "بطاقة جديدة",
		"cy" : "Cerdyn Newydd"
	},
	
	"Edit-Page-Title" :
	{
		"en" : "Edit Card",
		"fr" : "Modifier la Carte",
		"de" : "Bearbeiten Karte",
		"it" : "Modifica Carta",
		"es" : "Editar Tarjeta",
		"nl" : "Bewerk Kaart",
		"da" : "Ændre kortet",
		"no" : "Modifisere Kortet",
		"sv" : "Redigera Kortet",
		"fi" : "Muokkaa Kortin",
		"cs" : "Úpravy Karta",
		"lt" : "Redaguoti Kortelė",
		"pt" : "Editar Cartão",
		"pl" : "Edycja Karta",
		"ru" : "Редактировать карточку",
		"zh" : "编辑卡",
		"hi" : "संपादित कार्ड",
		"ja" : "編集カード",
		"el" : "Τροποποίηση Κάρτα",
		"ar" : "تحرير بطاقة",
		"cy" : "Golygu Cerdyn"
	},
	
	"Edit-Card-Name" :
	{
		"en" : "Card Name",
		"fr" : "Nom de la carte",
		"de" : "Kartenname",
		"it" : "Nome Di Carta",
		"es" : "Tarjeta de Nombre",
		"nl" : "Kaartnaam",
		"da" : "Kortnavn",
		"no" : "Kortnavn",
		"sv" : "Kortnamn",
		"fi" : "Kortin nimi",
		"cs" : "Titul Karty",
		"lt" : "Kortelės pavadinimas",
		"pt" : "Nome Do Cartão",
		"pl" : "Nazwa karta",
		"ru" : "Название карты",
		"zh" : "卡名称",
		"hi" : "कार्ड का नाम",
		"ja" : "カード名",
		"el" : "Όνομα κάρτας",
		"ar" : "اسم بطاقة",
		"cy" : "Enw y Cerdyn"
	},
	
	"Edit-Card-Number" :
	{
		"en" : "Card Number",
		"fr" : "Numéro de la carte",
		"de" : "Kartennummer",
		"it" : "Numero Di Carta",
		"es" : "Número De Tarjeta",
		"nl" : "Kaartnummer",
		"da" : "Kortnummer",
		"no" : "Kortnummer",
		"sv" : "Kortnummer",
		"fi" : "Kortin numero",
		"cs" : "Číslo Karty",
		"lt" : "Kortelės Numeris",
		"pt" : "Número Do Cartão",
		"pl" : "Numer Karta",
		"ru" : "Номер Карты",
		"zh" : "卡号",
		"hi" : "कार्ड नंबर",
		"ja" : "カード番号",
		"el" : "Αριθμός Κάρτας",
		"ar" : "رقم البطاقة",
		"cy" : "Rhif y Cerdyn"
	},
	
	"Edit-Card-Error-1" :
	{
		"en" : "Card number must have an even number of digits.",
		"fr" : "Le numéro de carte doit avoir un nombre pair de chiffres.",
		"de" : "Kartennummer muss eine gerade Anzahl von Ziffern.",
		"it" : "Numero della carta deve avere un numero pari di cifre.",
		"es" : "Número de tarjeta debe tener un número par de dígitos.",
		"nl" : "Kaartnummer moet een even aantal cijfers hebben.",
		"da" : "Kortnummer skal have et lige antal cifre.",
		"no" : "Kortnummer må ha et likt antall sifre.",
		"sv" : "Kortnummer måste ha ett jämnt antal siffror.",
		"fi" : "Kortin numero tulee olla parillinen määrä numeroita.",
		"cs" : "Číslo karty musí mít sudý počet číslic.",
		"lt" : "Kortelės numeris ilgis turi būti net.",
		"pt" : "Número do cartão deve ter um número par de dígitos.",
		"pl" : "Numer karty musi mieć parzystą liczbę cyfr.",
		"ru" : "Номер карты должен иметь четное число цифр.",
		"zh" : "卡号必须具有偶数个数字。",
		"hi" : "कार्ड नंबर अंक की एक भी नंबर होना चाहिए।",
		"ja" : "カード番号は偶数桁の番号を持っている必要があります。",
		"el" : "Τον αριθμό της κάρτας πρέπει να έχουν ζυγό αριθμό ψηφίων.",
		"ar" : "يجب أن يكون رقم البطاقة حتى عدد من الأرقام.",
		"cy" : "Rhaid Rhif y cerdyn gael hyd yn oed nifer o ddigidau."
	},
	
	"Edit-Card-Error-2" :
	{
		"en" : "Card number must have 12 or 13 digits.",
		"fr" : "Numéro de carte doit avoir 12 ou 13 chiffres.",
		"de" : "Kartennummer muss 12 oder 13 Ziffern.",
		"it" : "Numero della carta deve avere 12 o 13 cifre.",
		"es" : "Número de tarjeta debe tener 12 o 13 dígitos.",
		"nl" : "Kaartnummer moet 12 of 13 cijfers.",
		"da" : "Kortnummer skal have 12 eller 13 cifre.",
		"no" : "Kortnummer må ha 12 eller 13 sifre.",
		"sv" : "Kortnummer måste ha 12 eller 13 siffror.",
		"fi" : "Kortin numero on oltava 12 tai 13 numeroa.",
		"cs" : "Číslo karty musí mít 12 nebo 13 číslic.",
		"lt" : "Kortelės numeris turi būti 12 arba 13 skaitmenų.",
		"pt" : "Número do cartão deve ter 12 ou 13 dígitos.",
		"pl" : "Numer karty musi mieć 12 lub 13 cyfr.",
		"ru" : "Номер карты должен иметь 12 или 13 цифр.",
		"zh" : "卡号必须有十二三岁的数字。",
		"hi" : "कार्ड नंबर बारह या तेरह अंक होने चाहिए।",
		"ja" : "カード番号わずか12または13桁を持つことができます。",
		"el" : "Τον αριθμό της κάρτας πρέπει να έχει 12 ή 13 ψηφία.",
		"ar" : "يجب أن يكون رقم بطاقة اثني عشر أو ثلاثة عشر رقما.",
		"cy" : "Rhaid Rhif y cerdyn gael 12 neu 13 o ddigidau."
    },
    "Edit-Card-Error-3" :
    {
        "en" : "Card number must have 7 or 8 digits.",
        "fr" : "Numéro de carte doit avoir 7 ou 8 chiffres.",
        "de" : "Kartennummer muss 7 oder 8 Ziffern.",
        "it" : "Numero della carta deve avere 7 o 8 cifre.",
        "es" : "Número de tarjeta debe tener 7 o 8 dígitos.",
        "nl" : "Kaartnummer moet 7 of 8 cijfers.",
        "da" : "Kortnummer skal have 7 eller 8 cifre.",
        "no" : "Kortnummer må ha 7 eller 8 sifre.",
        "sv" : "Kortnummer måste ha 7 eller 8 siffror.",
        "fi" : "Kortin numero on oltava 7 tai 8 numeroa.",
        "cs" : "Číslo karty musí mít 7 nebo 8 číslic.",
        "lt" : "Kortelės numeris turi būti 7 arba 8 skaitmenų.",
        "pt" : "Número do cartão deve ter 7 ou 8 dígitos.",
        "pl" : "Numer karty musi mieć 7 lub 8 cyfr.",
        "ru" : "Номер карты должен иметь 7 или 8 цифр.",
        "zh" : "卡号必须有七位或八位数字。",
        "hi" : "कार्ड नंबर सात या आठ अंक होने चाहिए।",
        "ja" : "カード番号わずか7または8桁を持つことができます。",
        "el" : "Τον αριθμό της κάρτας πρέπει να έχει 7 ή 8 ψηφία.",
        "ar" : "يجب أن يكون رقم بطاقة سبعة أو ثمانية أرقام.",
        "cy" : "Rhaid Rhif y cerdyn gael 7 neu 8 o ddigidau."
    }
}

const language = navigator.language.substring (0,2);

function applyLocalisation ()
{
	var elements = document.querySelectorAll ("[data-i18n-key]");

	for (var i = 0; i < elements.length; i++)
	{
        var element = elements[i];
        var key = element.getAttribute ("data-i18n-key");
        var attr = element.getAttribute ("data-i18n-attribute");
        var localisation = translations[key];

        if (localisation === null || localisation === undefined) { continue };
      
        var translation = localisation[language];

        if (translation === null || translation === undefined) { continue; }

		if (!(attr === null || attr === undefined))
		{
            element.setAttribute (attr, translations[key][language]);
		}
		else
		{
            element.innerHTML = translations[key][language];
		}
	}
}
